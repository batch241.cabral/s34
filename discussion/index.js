const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/greet", (req, res) => {
    res.send("Hello from the /greet endpoint!");
});

app.post("/hello", (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
});
let users = [];
app.post("/signup", (req, res) => {
    if (
        req.body.userName !== "" &&
        req.body.passWord !== "" &&
        req.body !== ""
    ) {
        users.push(req.body);
        res.send(`User ${req.body.userName} successfully registered!`);
    } else {
        res.send("Please input username and password.");
    }
});

app.patch("/change-password", (req, res) => {
    let message;

    for (let i = 0; i < users.length; i++) {
        if ((req.body.userName = users[i].userName)) {
            users[i].passWord = req.body.passWord;

            message = `User ${req.body.userName}'s password has been updated`;
            break;
        } else {
            message = "User does not exist";
        }
    }
    res.send(message);
});

app.listen(port, () => console.log(`Server is runnin on port: ${port}`));
