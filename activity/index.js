const express = require("express");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const users = [
    {
        "username": "johndoe",
        "password": "johndoe123",
    },
];

app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
});

app.get("/users", (req, res) => {
    res.send(users);
});

app.delete("/delete-user", (req, res) => {
    let message;

    for (let i = 0; i < users.length; i++) {
        if (req.body.username === users[i].username) {
            users.splice(i, 1);

            message = `User ${req.body.username} has been deleted`;
            break;
        } else {
            message = "User not found";
        }
    }

    res.send(message);
});
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening to port: ${port}...`));
